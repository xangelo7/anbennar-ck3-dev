k_small_kingdom = {
	1021.10.3 = {
		holder = 26 #Finnic sil Vis
	}
}

k_beepeck = {
	1018.4.4 = {
		government = republic_government
		holder = 42 #Arnold Roysfort
	}
}

d_uelaire = {
	1015.6.7 = {
		holder = 43 #Eustace sil Uelaire
	}
}

k_roysfort = {
	990.4.5 = {
		holder = 44 #Barton Roysfort
	}
}

d_appleton = {
	1000.1.1 = {
		holder = 45 #Jon Appleseed
	}
}

d_pearview = {
	1000.1.1 = {
		holder = 46 #Frederic Peartree
	}
}

k_iochand = {
	1013.4.5 = {
		holder = 47 #Carwick Iochand
	}
}