﻿default=plains

#E_Anbennar

#K_Damescrown

#Damescrown
TBD

#Heartland Borders
TBD

#K_Esmaria

#Ryalanar
264=plains
265=plains
575=forest
917=forest

#Cann Esmar
259=plains
260=plains
266=plains
267=plains
911=hills

#Bennonhill
263=mountains
268=farmlands
270=mountains
271=plains
909=plains

#Konwell
269=plains
275=plains
905=forest

#Songbarges
274=farmlands
279=plains
333=plains
916=plains

#Hearthswood
272=forest
273=plains
900=forest

#Low Esmar
44=farmlands
280=farmlands
297=plains
301=hills
576=plains

#High Esmar
302=plains
308=forest
309=plains
901=farmlands
912=hills

#Ashfields
310=plains
311=plains
320=plains
902=plains

K_Verne

#Easneck
25=farmlands
45=plains
50=farmlands
51=plains

#Menibor Looop
284=farmlands
285=farmlands
331=farmlands
569=forest

#Galeinn
287=farmlands
295=farmlands
906=plains

#Heroes' Rest
283=plains
286=plains

#Verne
288=forest
289=forest
292=forest
293=plains

#Wyvernmark
290=plains
291=plains
838=hills

#K_Wex

#Wexhills
303=hills
304=hills
305=hills
306=hills

#Ottocam
307=hills
903=forest
907=plains

#Bisan
294=plains
300=plains
332=plains
579=farmlands
923=plains

#Gnollsgate
316=hills
317=hills
913=plains

#Greater Bardswood
318=forest
319=plains
918=forest

#Rhinmond
415=plains
919=plains

#K_Borders

#Gisden
312=plains
321=plains
326=plains
920=plains

#Arannen
313=forest
325=plains
418=forest
904=farmlands

#Eastborders
322=forest
323=plains
324=plains
421=plains

#Tellum
414=hills
417=hills

#Antir Drop
416=hills
419=hills
420=hills

#Hawkfields
409=plains
410=plains
411=plains

#Highcliff
412=plains
413=forest
921=plains
922=forest

#K_Dameria

#Damesear
2=hills		#Eargate
3=plains	#Old Damenath
4=mountains	#Temple of the Highest Moon
5=forest	#Cymlan Docks
6=farmlands	#Damerian Fields
8=farmlands #Anbenncost
9=hills 	#Moonisle
11=plains	#Middletooth
168=plains	#Auraire

#Exwes
18=hills	
153=plains
154=plains
156=plains

#Wesdam
10=farmlands #Wesdam
13=plains	#Toothsend
15=farmlands #Gabelaire
17=plains	#Lenceiande
20=forest	#Ordoin
28=hills	#Modgate
31=forest	#Ilvandet

#Neckcliffe
16=farmlands #Windtower
21=forest	#Dockbride
23=plains	#Triancost	
24=plains	#Stoneview
26=farmlands #Throatport
27=plains	#Neckcliffe

#K Perlsedge

#Pearlsedge
14=hills	#the Pearls
42=forest	#Earngrove
43=farmlands #Pearlsedge
46=plains	#Jewelpoint

#Pearlywine
41=forest
49=forest

#Acromton
276=hills
330=hills

#Istralore
37=hills
277=hills
899=hills

#Silverwoods
29=hills
52=farmlands
53=forest
54=forest
55=forest
56=forest

#Plumwall
281=plains
328=plains
910=plains

#Heartlands
278=plains
282=plains
296=plains
394=plains

#Middle Luna
109=farmlands
329=plains
587=plains

#Upper Luna
298=farmlands
299=hills
327=farmlands
898=forest

#K_Carneter

#Carneter
19=forest	#Carneter
22=forest	#Woodwell
30=forest	#Ancard's Crossing
57=forest	#Timberfort

#K_Tretun

#Tretun
47=plains
48=hills

#Roilsard
32=hills
33=farmlands
34=forest
40=plains
59=forest

#E_Lencenor

#K_Rubyhold

#Rubyhold
62=mountains
64=mountains
65=mountains

#K_Sorncost

#Coruan
84=hills
86=hills
89=plains

#Sormanni Hills
91=hills
100=hills

#Sorncost
85=hills
88=hills
90=hills
99=hills

#K_Deranne

#Deranne
110=forest
111=plains
112=plains
113=plains
121=plains

#Darom
58=farmlands
102=farmlands
104=plains

#K_Lorent

#Lorentaine
67=farmlands
69=mountains
70=plains

#Lorenith
61=forest
68=plains

#Ainethan
38=plains
72=forest
73=plains

#Upper Bloodwine
74=plains
95=forest
108=plains
114=farmlands

#Lower Bloodwine
79=plains
80=hills
81=hills
97=plains
101=farmlands

#Enteben
96=plains
98=hills

#Horsegarden
78=plains
105=plains

#Crovania
82=plains
83=farmlands
106=hills

#Venail
93=forest
94=plains
127=forest

#Great Ording
75=plains
92=plains
107=plains

#Rewanwood
77=plains
87=plains
119=plains

#Redglades
115=forest
116=forest
117=forest
118=forest
120=forest

#Rosefield
71=mountains
133=plains
152=hills
158=hills

#E_Small_Country(not in)

#K_Roysfort

#Roysfort
130=plains
131=plains
132=plains

#Bigwheat
134=forest
136=plains

#K_Ciderfield

#Appleton
137=farmlands
139=forest
163=farmlands

#Pearview
103=farmlands
147=farmlands
165=farmlands

#K_Small_Kingdom

#Thomsbridge
135=farmlands
159=farmlands
166=plains

#Barrowshire
160=hills
161=hills
162=farmlands
164=farmlands

#Greymill
12=farmlands
76=forest
149=forest

#Viswall
63=hills
66=farmlands

#Elkmarch
7=forest
206=forest
210=forest
348=forest

#Uelaire
207=plains
208=forest
209=plains
347=plains

#K_Beepeck

#Beepeck
60=wetlands
151=farmlands
155=wetlands
157=farmlands
205=farmlands

#K_Iochand

#Southroy
123=plains
124=plains
125=plains
128=plains

#Portnamm
126=farmlands

#Iochand
129=plains
141=farmlands
142=plains
143=plains

#K_Reveria

#Reaver Coast
122=plains
138=forest
140=forest

#Gnomish Pass
144=plains
145=plains
148=mountains
167=hills

#K_Nimscodd
169=hills
188=hills
190=hills
199=hills

#E_Alenic_Empire

#K_Eaglecrest

#Dragonhills
150=mountains
211=mountains
212=forest
213=hills
336=hills

#Floodmarches
214=wetlands
215=wetlands

#K_Westmoors

#Westmoor
191=hills
192=wetlands
193=wetlands
194=wetlands
345=wetlands

#Moorhills
195=hills
196=hills

#Beronmoor
201=wetlands
202=wetlands
203=wetlands
204=wetlands

#E_Businor

#K_Busilar

#Khenak
375=mountains
376=hills
377=mountains

#Lorinhap
374=hills
378=plains

#Busilari Straits
39=plains
372=plains
373=plains

#Lioncost
365=hills
367=hills
368=plains

#Hapiande
370=plains
371=hills

#Lorbet
366=hills
408=plains

#Mountainway
362=plains
363=forest
364=mountains

#K_Eborthil

#Eborthil
369=hills
380=hills

#Tefkora
35=hills
36=hills
379=hills
