﻿anbenncost = {
	county = c_anbenncost
	barony = b_anbenncost
	
	character_modifier = {
		name = holy_site_anbenncost_effect_name
		development_growth_factor = 0.1
		diplomacy = 1
	}
}

moonmount = {
	county = c_moonmount
	barony = b_temple_of_the_highest_moon
	
	character_modifier = {
		name = holy_site_moonmount_effect_name
		learning_per_piety_level = 1
	}
}