﻿# Accepted categories are: governments, cultures, culture_groups, faiths, religions
# A Flavorization will apply if ALL specified categories have ANY of their entries
# represented in the character/title
# So for example:
# governments = { feudal_government }
# culture_groups = { mongolic_group central_germanic_group }
# religions = { christianity_religion }
# cultures = { norse }
# faiths = { catholicism }
# For this to apply a character MUST BE feudal, AND must also be EITHER altaic OR germanic
# priority is used to determine priority, if they are the same then the LAST will be selected,
# and honorifics will be prefered over landed titles. Default priority is 1
# If a flavorization does not have a tier it can apply to all tiers of characters, too many non-tiered
# flavorizations is bad for performance though, YOU HAVE BEEN WARNED!
# 'top_liege = no' means that its the holder of the title causing the honorific that is tested for
# the various categories, so for a prince it is the king that is tested, for a queen mother it is her son
# if this is not specified ( or 'top_liege = yes' the default ) then it is the top liege of that character
# that is tested, this means that most title flavor comes from the top liege of the title not the holder

#sultan = { #Example
#	gender = male
#	special = holder
#	tier = kingdom
	#	governments = { feudal_government clan_government }
#	religions = { islam_religion }
#}

#############################################

######## TITLE-BASED ########


#Dukes Crossguard of Acromton, Kings if independent
duke_feudal_male_d_acromton = { # Duke-Crossguard
	type = character
	gender = male
	special = holder
	priority = 48	#FYI the usual is 47 for alternates
	governments = { feudal_government }
	titles = { d_acromton }
	top_liege = no
}

duke_feudal_female_d_acromton = { 
	type = character
	gender = female
	special = holder
	priority = 48
	governments = { feudal_government }
	titles = { d_acromton }
	top_liege = no
}

duke_feudal_male_d_acromton = { # Duke-Crossguard
	type = character
	gender = male
	special = holder
	priority = 48	#FYI the usual is 47 for alternates
	governments = { feudal_government }
	titles = { d_acromton }
	top_liege = no
}

duke_feudal_female_d_acromton = { 
	type = character
	gender = female
	special = holder
	priority = 48
	governments = { feudal_government }
	titles = { d_acromton }
	top_liege = no
}


#High King of Lencenor, like Lorenan
emperor_feudal_male_e_lencenor = { # High King
	type = character
	gender = male
	special = holder
	priority = 48	#FYI the usual is 47 for alternates
	governments = { tribal_government clan_government feudal_government }
	titles = { e_lencenor }
	only_independent = yes
	top_liege = no
}

emperor_feudal_female_e_lencenor = { # High Queen
	type = character
	gender = female
	special = holder
	priority = 48
	governments = { tribal_government clan_government feudal_government }
	titles = { e_lencenor }
	only_independent = yes
	top_liege = no
}

#Moor King of Westmoors
king_feudal_male_k_westmoors_moorman = { # Moor King
	type = character
	gender = male
	special = holder
	priority = 48	#FYI the usual is 47 for alternates
	cultures = { moorman }
	governments = { tribal_government clan_government feudal_government }
	titles = { k_westmoors }
	only_independent = yes
	top_liege = no
}

king_feudal_female_k_westmoors_moorman = { # Moor Queen
	type = character
	gender = female
	special = holder
	priority = 48
	cultures = { moorman }
	governments = { tribal_government clan_government feudal_government }
	titles = { k_westmoors }
	only_independent = yes
	top_liege = no
}

#King of Iochand
king_feudal_male_k_iochand_gnomish_group = { # Iochand is royal title, this is to override Hierarchs and such
	type = character
	gender = male
	special = holder
	priority = 48
	culture_groups = { gnomish_group }
	governments = { tribal_government clan_government feudal_government }
	titles = { k_iochand }
	only_independent = yes
	top_liege = no
}

king_feudal_female_k_iochand_gnomish_group = { # Iochand is royal title, this is to override Hierarchs and such
	type = character
	gender = female
	special = holder
	priority = 48
	culture_groups = { gnomish_group }
	governments = { tribal_government clan_government feudal_government }
	titles = { k_iochand }
	only_independent = yes
	top_liege = no
}

kingdom_feudal_k_iochand = {
	type = title
	tier = kingdom
	priority = 48
	governments = { tribal_government clan_government feudal_government }
	culture_groups = { gnomish_group }
	titles = { k_iochand }
	only_independent = yes
	top_liege = no
}

######## CULTURAL ###########

#Moon Elf - Feudal
duke_feudal_male_moon_elf = {
	type = character
	gender = male
	special = holder
	tier = duchy
	priority = 27
	governments = { feudal_government }
	cultures = { moon_elvish }
	top_liege = no	#BTW this is to make them Princes even when they are still vassals. If this is not here then they default to Dukes
}
duke_feudal_female_moon_elf = {
	type = character
	gender = female
	special = holder
	tier = duchy
	priority = 27
	governments = { feudal_government }
	cultures = { moon_elvish }
	top_liege = no
}
duchy_feudal_moon_elf = {
	type = title
	tier = duchy
	priority = 27
	governments = { feudal_government }
	cultures = { moon_elvish }
	top_liege = no
}
king_feudal_male_moon_elf = {
	type = character
	gender = male
	special = holder
	tier = kingdom
	priority = 47
	governments = { feudal_government }
	cultures = { moon_elvish }
	top_liege = no
}
king_feudal_female_moon_elf = {
	type = character
	gender = female
	special = holder
	tier = kingdom
	priority = 47
	governments = { feudal_government }
	cultures = { moon_elvish }
	top_liege = no
}
kingdom_feudal_moon_elf = {
	type = title
	tier = kingdom
	priority = 47
	governments = { feudal_government }
	cultures = { moon_elvish }
	top_liege = no
}
emperor_feudal_male_moon_elf = {
	type = character
	gender = male
	special = holder
	tier = empire
	priority = 102
	governments = { feudal_government }
	cultures = { moon_elvish }
	top_liege = no
}
emperor_feudal_female_moon_elf = {
	type = character
	gender = female
	special = holder
	tier = empire
	priority = 102
	governments = { feudal_government }
	cultures = { moon_elvish }
	top_liege = no
}
empire_feudal_moon_elf = {
	type = title
	tier = empire
	priority = 102
	governments = { feudal_government }
	cultures = { moon_elvish }
	top_liege = no
}


#Alenic - Feudal
count_feudal_male_alenic_group = {	#Lord
	type = character
	gender = male
	special = holder
	tier = county
	priority = 27
	governments = { feudal_government }
	culture_groups = { alenic_group }
}
count_feudal_female_alenic_group = {
	type = character
	gender = female
	special = holder
	tier = county
	priority = 27
	governments = { feudal_government }
	culture_groups = { alenic_group }
}
county_feudal_alenic_group = {
	type = title
	tier = county
	priority = 27
	governments = { feudal_government }
	culture_groups = { alenic_group }
}
duke_feudal_male_alenic_group = {	#Great Lord
	type = character
	gender = male
	special = holder
	tier = duchy
	priority = 27
	governments = { feudal_government }
	culture_groups = { alenic_group }
}
duke_feudal_female_alenic_group = {
	type = character
	gender = female
	special = holder
	tier = duchy
	priority = 27
	governments = { feudal_government }
	culture_groups = { alenic_group }
}
duchy_feudal_alenic_group = {
	type = title
	tier = duchy
	priority = 27
	governments = { feudal_government }
	culture_groups = { alenic_group }
}


#Gnomish - Republic (Make em Hierarchies)
king_republic_male_gnomish_group = {
	type = character
	gender = male
	special = holder
	tier = kingdom
	priority = 47
	governments = { republic_government }
	culture_groups = { gnomish_group }
}
king_republic_female_gnomish_group = {
	type = character
	gender = female
	special = holder
	tier = kingdom
	priority = 47
	governments = { republic_government }
	culture_groups = { gnomish_group }
}
kingdom_republic_gnomish_group = {
	type = title
	tier = kingdom
	priority = 27
	governments = { republic_government }
	culture_groups = { gnomish_group }
}
emperor_republic_male_gnomish_group = {
	type = character
	gender = male
	special = holder
	tier = empire
	priority = 47
	governments = { republic_government }
	culture_groups = { gnomish_group }
}
emperor_republic_female_gnomish_group = {
	type = character
	gender = female
	special = holder
	tier = empire
	priority = 47
	governments = { republic_government }
	culture_groups = { gnomish_group }
}
empire_republic_gnomish_group = {
	type = title
	tier = empire
	priority = 27
	governments = { republic_government }
	culture_groups = { gnomish_group }
}

#Gnomish - republic (Make em Hierarchies)
king_republic_male_gnomish_group = {
	type = character
	gender = male
	special = holder
	tier = kingdom
	priority = 47
	governments = { republic_government }
	culture_groups = { gnomish_group }
}
king_republic_female_gnomish_group = {
	type = character
	gender = female
	special = holder
	tier = kingdom
	priority = 47
	governments = { republic_government }
	culture_groups = { gnomish_group }
}
kingdom_republic_gnomish_group = {
	type = title
	tier = kingdom
	priority = 27
	governments = { republic_government }
	culture_groups = { gnomish_group }
}
emperor_republic_male_gnomish_group = {
	type = character
	gender = male
	special = holder
	tier = empire
	priority = 47
	governments = { republic_government }
	culture_groups = { gnomish_group }
}
emperor_republic_female_gnomish_group = {
	type = character
	gender = female
	special = holder
	tier = empire
	priority = 47
	governments = { republic_government }
	culture_groups = { gnomish_group }
}
empire_republic_gnomish_group = {
	type = title
	tier = empire
	priority = 27
	governments = { republic_government }
	culture_groups = { gnomish_group }
}