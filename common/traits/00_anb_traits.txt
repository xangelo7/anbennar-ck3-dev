#Pure Elf
elf = {
	index = 300
	
	fertility = -0.75
	learning = 4
	diplomacy = 5
	
	attraction_opinion = 20
	
	vassal_opinion = 15
	same_opinion = 20
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	immortal = yes
	
	genetic_constraint_all = beauty_2
	genetic_constraint_men = male_beauty_2
	genetic_constraint_women = female_beauty_2
	forced_portrait_age_index = 1 # old_beauty_1
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = consecrated_blood.dds
			}
			desc = consecrated_blood.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_elf_desc
			}
			desc = trait_elf_character_desc
		}
	}
}

#Half-Elf
half_elf = {
	index = 301
	
	fertility = -0.3
	learning = 3
	diplomacy = 3
	
	attraction_opinion = 10
	
	vassal_opinion = 10
	same_opinion = 20
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	genetic_constraint_all = beauty_2
	genetic_constraint_men = male_beauty_2
	genetic_constraint_women = female_beauty_2
	forced_portrait_age_index = 1 # old_beauty_1

	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = blood_of_prophet.dds
			}
			desc = blood_of_prophet.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_half_elf_desc
			}
			desc = trait_half_elf_character_desc
		}
	}
}

# #Elf Blooded
# elf_blood = {
	# index = 302
	
	# fertility -0.1
	# learning = 2
	# diplomacy = 2
	
	# attraction_opinion = 10
	
	# vassal_opinion = 5
	# same_opinion = 10
	
	# inherit_chance = 100
	# both_parent_has_trait_inherit_chance = 100
	# physical = yes
	
	# birth = 0
	# random_creation = 0
	
	# icon = {
		# first_valid = {
			# triggered_desc = {
				# trigger = { NOT = { exists = this } }
				# desc = blood_of_the_prophet_parent.dds
			# }
			# desc = blood_of_the_prophet_parent.dds
		# }
	# }	
	# desc = {
		# first_valid = {
			# triggered_desc = {
				# trigger = {
					# NOT = { exists = this }
				# }
				# desc = trait_elf_blood_desc
			# }
			# desc = trait_elf_blood_character_desc
		# }
	# }
# }

#Human
human = {
	index = 303
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_human_desc
			}
			desc = trait_human_character_desc
		}
	}
}

#Dwarf
anb_dwarf = {
	index = 304
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	immortal = yes
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = sea_raider.dds
			}
			desc = sea_raider.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_anb_dwarf_desc
			}
			desc = trait_anb_dwarf_character_desc
		}
	}
}

#Halfling
halfling = {
	index = 305
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = dwarf.dds
			}
			desc = dwarf.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_halfling_desc
			}
			desc = trait_halfling_character_desc
		}
	}
}

#Gnome
gnome = {
	index = 306
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	immortal = yes
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = bossy.dds
			}
			desc = bossy.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_gnome_desc
			}
			desc = trait_gnome_character_desc
		}
	}
}

#Gnomling
gnomling = {
	index = 307
	
	can_have_children = no
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = bossy.dds
			}
			desc = bossy.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_gnomling_desc
			}
			desc = trait_gnomling_character_desc
		}
	}
}

#Orc
orc = {
	index = 308
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_orc_desc
			}
			desc = trait_orc_character_desc
		}
	}
}

#Half-Orc
half_orc = {
	index = 309
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_half_orc_desc
			}
			desc = trait_half_orc_character_desc
		}
	}
}

#Gnoll
gnoll = {
	index = 310
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_gnoll_desc
			}
			desc = trait_gnoll_character_desc
		}
	}
}

#Kobold
kobold = {
	index = 311
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_kobold_desc
			}
			desc = trait_kobold_character_desc
		}
	}
}

#Troll
troll = {
	index = 312
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_troll_desc
			}
			desc = trait_troll_character_desc
		}
	}
}

#Trollkin
trollkin = {
	index = 313
	
	can_have_children = no
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_trollkin_desc
			}
			desc = trait_trollkin_character_desc
		}
	}
}

#Ogre
ogre = {
	index = 314
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_ogre_desc
			}
			desc = trait_ogre_character_desc
		}
	}
}

#Ogrillon
ogrillon = {
	index = 315
	
	fertility = -0.5
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_ogrillon_desc
			}
			desc = trait_ogrillon_character_desc
		}
	}
}

#Goblin
goblin = {
	index = 316
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_goblin_desc
			}
			desc = trait_goblin_character_desc
		}
	}
}

#Half-goblin
half_goblin = {
	index = 317
	
	can_have_children = no
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_half_goblin_desc
			}
			desc = trait_half_goblin_character_desc
		}
	}
}

#Hobgoblin
hobgoblin = {
	index = 318
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_hobgoblin_desc
			}
			desc = trait_hobgoblin_character_desc
		}
	}
}

#Half-hobgoblin
half_hobgoblin = {
	index = 319
	
	can_have_children = no
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_half_hobgoblin_desc
			}
			desc = trait_half_hobgoblin_character_desc
		}
	}
}

#Lesser Hobgoblin
lesser_hobgoblin = {
	index = 320
	
	fertility = -0.5
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_lesser_hobgoblin_desc
			}
			desc = trait_lesser_hobgoblin_character_desc
		}
	}
}

#Harimari
harimari = {
	index = 321
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_harimari_desc
			}
			desc = trait_harimari_character_desc
		}
	}
}

#Centaur
centaur = {
	index = 322
	
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_centaur_desc
			}
			desc = trait_centaur_character_desc
		}
	}
}

#Harpy
harpy = {
	index = 323
	
	fertility = 1	#Since half of pregnancies will end early due to being boys
	learning = 2
	diplomacy = 2

	same_opinion = 10
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_harpy_desc
			}
			desc = trait_harpy_character_desc
		}
	}
}