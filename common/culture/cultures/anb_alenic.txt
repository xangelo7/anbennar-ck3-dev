﻿alenic_group = {
	
	graphical_cultures = {
		german_group_coa_gfx
		western_coa_gfx
		western_building_gfx
		northern_clothing_gfx
		western_unit_gfx
	}
	
	blue_reachmen = {	#Consider this to be "old frisian", in the times of "Frankish" culture, so 760s and before.
		# Effectively, this culture isn't playable in game
	
		graphical_cultures = {
			frisian_coa_gfx
		}
		
		color = { 80 107 232 }

		dynasty_names = {
            "dynn_Abdena"
            "dynn_Allena"
            "dynn_Andringa"
            "dynn_Bauwenga"
            "dynn_Beninga"
            "dynn_Boltinge"
            "dynn_Botta"
            "dynn_Buning"
            "dynn_Butzel"
            "dynn_Cammingha"
            "dynn_Cirksema"
            "dynn_Clant"
            "dynn_Coenders"
            "dynn_Cordinghe"
            "dynn_Dives"
            "dynn_Ewsum"
            "dynn_Folkerdinga"
            "dynn_Gaykema"
            "dynn_Ghetzerka"
            "dynn_Gockinga"
            "dynn_Goltraven"
            "dynn_Haijkama"
            "dynn_Harting"
            "dynn_Heddama"
            "dynn_Hedegu"
            "dynn_Hedinge"
            "dynn_Hornada"
            "dynn_Hornekingh"
            "dynn_Houwerda"
            "dynn_Huginge"
            "dynn_Huninga"
            "dynn_Huwenga"
            "dynn_Idzarda"
            "dynn_Ildsisma"
            "dynn_Jarges"
            "dynn_Kalemere"
            "dynn_Kater"
            "dynn_Korenporta"
            "dynn_Kote"
            "dynn_Lewe"
            "dynn_Manninga"
            "dynn_Martena"
            "dynn_Meckema"
            "dynn_Mernseta"
            "dynn_Niding"
            "dynn_Oenema"
            "dynn_Omken"
            "dynn_Ommeloop"
            "dynn_Onsta"
            "dynn_Papinge"
            "dynn_Poder"
            "dynn_Pol"
            "dynn_Popinghe"
            "dynn_Potens"
            "dynn_Reding"
            "dynn_Remian"
            "dynn_Rengers"
            "dynn_Rickwardsma"
            "dynn_Ripperda"
            "dynn_Rodmersma"
            "dynn_Schulte"
            "dynn_Serda"
            "dynn_Sickinge"
            "dynn_Spegel"
            "dynn_Stickel"
            "dynn_Suanckerus"
            "dynn_Swaneke"
            "dynn_Tayingha"
            "dynn_Tiddinga"
            "dynn_Tjarksena"
            "dynn_Ukena"
            "dynn_Umbelap"
            "dynn_Wiemken"
            { "dynnp_de" "dynn_Aggere" }
            { "dynnp_de" "dynn_Helpman" }
            { "dynnp_de" "dynn_Mepsche" }
            { "dynnp_van" "dynn_Hallum" }
        }

		male_names = {

			#EU4
			Adelar Alain Alen Arnold Camor Carlan Clothar Colyn Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
			Frederic Humac Humban Humbar Humbert Lain Lan Madalac Marcan Petran Peyter Rabac Rabard Rican Rycan Ricard Rogec 
			Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Venac Vencan Walter Welyam Wystan
			Caylen Arman Adrien Adrian 

			#New
			Alenn Alann Lennic Lennard Jon Artur Eric Ben Bernard
			Garret Garrett Abram Alan Archibald Barnabas Bennett Brandon Branden Brenden Byron Byrne Thorburn Nathaniel

			Hugh Peter Ralph Robert Roger Simon Thomas Walter
			Alan Albert Alfred Andrew Anselm Arnold Baldrick David Edmund Edward Eric George Gerald Gilbert
			Gregory Humphrey Jordan Liel Mark Martin Matthew
			Randolph Reginald Stephen Teague Waleran

			Alf Anders Anundrne Arnfast Astrad Azur Bagge Balder Bertil Birger Borkvard Botulf Brage Bror
			Brynjolf Dag Dan Dyre Egil Einar Emund Erik Eskild Faste Fredrik Filip Folke Frej Georg Greger Grim GudrO_d Gunnar Gustav Halsten Hans Harald
			Helgi Henrik Hjalmar Holmger HA_kan Inge Ingemar Ingvar Jedvard Johan Karl Kettil Klas Knut Kol KolbjO_rn Lars Markus
			Mats Nils Niklas Odd Olaf Orvar Peder Ragnar Rolf RO_rek Ragnvald SA_mund SO_rkver SigbjO_rn Sigurd Starkad Sten Sune Sven
			Sverker Tolir Torbrand Torfinn Torkel Torolf Torsten Toste Totil Tyke Ulf Valdemar
			Adalvard Rodulvard Stenar Brynolf

			#Vanilla - from Old Saxon
			Arnold
			Bertold Bertram Boddic Brun
			Dethard Detmar Donar Eggerd
			Ghert
			Hoger Humfried Jacob Jaspar
			Lembert
			Luder Marbold Mathias Michael
			Rudolf Steffen
			Wulff

			Osric

		}

		female_names = {
			#EU4
			Bella Clarya Constance Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
			Coraline Cora Corina Cecille Athana

			#New
			Abigail Allison Annabel Belinda Bridget Deidre Heather Meade Melinda Rosamond Sybil 

			#old saxon
			Addila Agnes Anna Athela Bertha Bertrada Bia
			Brigida Elisabeth Enda Frederuna Geilana Gerberga Gertrud Gisela
			Hadwig Hasala Helene Hildegard Ida Imma
			Katharina Margarete Mathilde Oda

			Agatha Balthild Judith Mildrith  
			Margaret

			Beatrice Blanche Catherine Edith Ela Emma Isabel
			Isabella Juliana Margaret Mary Matilda Maud Sybilla

			Aleta Anna Asta Astrid Beata Birgitta Bothild Cecilia Edla Elin Elisabet Freja Gunhild Gyla Gurli Gytha Hafrid
			Helena Holmfrid Iliana Inga Ingeborg Ingegerd Ingfrid Ingrid Karin Katarina Kristina Linda Maer
			Margareta MAErta Ragnfrid Ragnhild Rikissa Saga Sara Sigrid Sofia Svanhild Thordis Ulvhilde Ylva A_sa

		}

		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 5
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 20
		mat_grm_name_chance = 40
		mother_name_chance = 5

		ethnicities = {
			25 = caucasian_blond
			5 = caucasian_ginger
			10 = caucasian_brown_hair
			60 = caucasian_dark_hair
		}
	}
	
	old_alenic = {	#Peoples living in the Greatwoods and along the remote parts of the Alenic Frontier
	
		graphical_cultures = {
			frisian_coa_gfx
		}

		color = { 82  130  152 }

		cadet_dynasty_names = {
			{ "dynnp_van" "dynn_Loon" }
			{ "dynnp_von" "dynn_Geldern" }
			{ "dynnp_van" "dynn_Bonen" }
			{ "dynnp_van" "dynn_Sulzbach" }
			{ "dynnp_van" "dynn_Wassemberg" }
			{ "dynnp_van" "dynn_Geldern" }
			{ "dynnp_van" "dynn_Bergh" }
			{ "dynnp_van" "dynn_Brabant" }
			{ "dynnp_van" "dynn_Namen" }
			{ "dynnp_van" "dynn_Loon" }
			{ "dynnp_van" "dynn_Heinsberg" }
			"dynn_De_Graeff"
			{ "dynnp_van" "dynn_Susa" }
			"dynn_Groth"
		}

		dynasty_names = {
			{ "dynnp_van" "dynn_Loon" }
			{ "dynnp_von" "dynn_Geldern" }
			{ "dynnp_van" "dynn_Bonen" }
			{ "dynnp_van" "dynn_Sulzbach" }
			{ "dynnp_van" "dynn_Wassemberg" }
			{ "dynnp_van" "dynn_Geldern" }
			{ "dynnp_van" "dynn_Bergh" }
			{ "dynnp_van" "dynn_Brabant" }
			{ "dynnp_van" "dynn_Namen" }
			{ "dynnp_van" "dynn_Loon" }
			{ "dynnp_van" "dynn_Heinsberg" }
			"dynn_De_Graeff"
			{ "dynnp_van" "dynn_Susa" }
			"dynn_Groth"
		}
		
		male_names = {

			#EU4
			Acromar Adelar Alain Alen Arnold Camir Camor Canrec Carlan Celgal Ciramod Clarimond Clothar Colyn Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
			Elecast Frederic Godrac Godric Godryc Godwin Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Rycan Ricard Rogec 
			Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Ulric Venac Vencan Walter Welyam Wystan Bellac
			Caylen Arman Adrien Adrian

			#New
			Alenn Alann Lennic Lennard

			#Vanilla - from Old Saxon
			Abo Adalgar Adalgod Aelle Alebrand
			Altfrid Ansgar Arnd Arnold Asig Beneke Benno
			Bernhard Bertold Bertram Boddic Brun Burchard Cobbo Cord
			Dethard Detmar Donar Eggerd
			Erik Esiko Everd Ewald Freawine Gero Gerold Gevert
			Ghert Giselher Giselmar Goswin Hartwig
			Hengest Henneke Herberd Hinrik Hoger Hulderic Humfried Immed Jacob Jaspar Jochim
			Lembert Liemar Liudger
			Luder Ludwig Marbold Marquard Mathias Merten Michael
			Norbert Odo Ordulf Radke Reginbern Reinbern Reineke Reinmar Rudigar
			Rudolf Steffen Thankmar Theoderic Theodoric
			Theodwin Thimo Tobe Tymmeke Udo Unwan Viric Volkwin Volrad Walbert Waldemar Waldered Waltard
			Warin Wecta Wenzel Werneke Wichbert Wichmann Wicho Wigebert Wilbrand
			Wulff

			Beorn Eadric Eardwulf Eastmund Guthmund Leofric Morcar Ordgar Osmund Osric Wulf Wulfgar Wulfgeat Wulfhelm Wulfhere Cenwulf Ceolwulf Beornwulf
			Werestan Eadbald Eormenric
		}

		female_names = {
			#EU4
			Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Magda Matilda Adela
			Coraline

			#old saxon
			Addila Adelheid Alof Agnes Anna Athela Beatrix Bertha Bertrada Bia Bisina Bithild
			Brigida Christina Diedke Eilika Elisabeth Enda Frederuna Geilana Gerberga Gertrud Gisela
			Glismod Hadwig Hasala Heilwig Helene Hildegard Hrothwina Ida Imma Irmgard Irminburg
			Jutta Katharina Kunigunde Luitgard Margarete Mathilde Mechthild Oda

			#For Gawedi
			Agatha Balthild Ecgfrida Judith Mildrith  
			Wulfhild Margaret Wulfwynn 	 

		}
		
		dynasty_of_location_prefix = "dynnp_of"
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 25
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 20
		mat_grm_name_chance = 40
		mother_name_chance = 5

		ethnicities = {
			30 = caucasian_blond
			10 = caucasian_brown_hair
			60 = caucasian_dark_hair
		}

		mercenary_names = {
			{ name = "mercenary_company_of_the_brabancon_wolf" coat_of_arms = "mc_company_of_the_brabancon_wolf" }
			{ name = "mercenary_company_flemish_band" }
			{ name = "mercenary_false_earls_company" }
		}
	}

	gawedi = {
		graphical_cultures = {
			english_coa_gfx
		}
		
		color = "gawedi_blue"

		cadet_dynasty_names = {
			{ "dynnp_de" "dynn_Bohun" }
			{ "dynnp_de" "dynn_Vere" }
			{ "dynnp_de" "dynn_Gand" }
			"dynn_Myall"
		}

		dynasty_names = {
			{ "dynnp_de" "dynn_Bohun" }
			{ "dynnp_de" "dynn_Vere" }
			{ "dynnp_de" "dynn_Gand" }
			"dynn_Myall"
		}
		

		male_names = {

			#EU4
			Adelar Alain Alen Arnold Camor Carlan Clothar Colyn Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
			Frederic Godrac Godric Godryc Godwin Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Rycan Ricard Rogec 
			Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Venac Vencan Walter Welyam Wystan
			Caylen Arman Adrien Adrian 

			#New
			Alenn Alann Lennic Lennard Jon Artur Eric Ben Bernard
			Garret Garrett Abram Alan Archibald Barnabas Bennett Brandon Branden Brenden Byron Byrne Thorburn Nathaniel

			Hugh Peter Ralph Robert Roger Simon Thomas Walter
			Alan Albert Alfred Andrew Anselm Arnold Baldrick David Edmund Edward Eric George Gerald Gilbert
			Gregory Humphrey Jordan Liel Mark Martin Matthew
			Randolph Reginald Stephen Teague Waleran

			#Vanilla - from Old Saxon
			Arnold
			Bertold Bertram Boddic Brun
			Dethard Detmar Donar Eggerd
			Ghert
			Hoger Humfried Jacob Jaspar
			Lembert
			Luder Marbold Mathias Michael
			Rudolf Steffen
			Wulff

			Osric

		}

		female_names = {
			#EU4
			Bella Clarya Constance Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
			Coraline Cora Corina Cecille Athana

			#New
			Abigail Allison Annabel Belinda Bridget Deidre Heather Meade Melinda Rosamond Sybil 

			#old saxon
			Addila Agnes Anna Athela Bertha Bertrada Bia
			Brigida Elisabeth Enda Frederuna Geilana Gerberga Gertrud Gisela
			Hadwig Hasala Helene Hildegard Ida Imma
			Katharina Margarete Mathilde Oda

			Agatha Balthild Judith Mildrith  
			Margaret

			Beatrice Blanche Catherine Edith Ela Emma Isabel
			Isabella Juliana Margaret Mary Matilda Maud Sybilla

		}

		dynasty_of_location_prefix = "dynnp_of"
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 25
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 20
		mat_grm_name_chance = 40
		mother_name_chance = 5
		
		ethnicities = {
			15 = caucasian_blond
			10 = caucasian_brown_hair
			75 = caucasian_dark_hair
		}

		mercenary_names = {
			{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
			{ name = "mercenary_company_hawkwoods_band" }
		}
	}

	#Elvenized Gawedi - Non Canon!
	nuralenic = {
		graphical_cultures = {
			english_coa_gfx
		}
		
		color = "gawedi_blue"

		cadet_dynasty_names = {
			{ "dynnp_de" "dynn_Bohun" }
			{ "dynnp_de" "dynn_Vere" }
			{ "dynnp_de" "dynn_Gand" }
			"dynn_Myall"
		}

		dynasty_names = {
			{ "dynnp_de" "dynn_Bohun" }
			{ "dynnp_de" "dynn_Vere" }
			{ "dynnp_de" "dynn_Gand" }
			"dynn_Myall"
		}
		
		male_names = {

			#EU4
			Adelar Alain Alen Arnold Camor Carlan Colyn Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
			Godric Henric Humban Humbar Humbert Lain Lan Madalac Marcan Petran Ricard 
			Stovan Teagan Toman Tomar Venac Vencan Walter Welyam
			Caylen Arman Adrien Adrian 

			Adran Albert Aldred Alfons Alfred Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Brandon Brayan Brayden Calas 
			Casten Castín Corin Daran Darran Denar Edmund Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Olor Oto Rean Ricain 
			Rion Robin Rogier Sandur Taelar Thal Thiren Tomas Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery Lucian 
			Dominic Vernell Arman Cecill Tristan Alvar Adelar Teagan Andrel Frederic Adrien Adrian Ademar Valeran Valen Emil

			#New
			Alenn Alann Lennic Lennard Jon Artur Eric Ben Bernard
			Garret Garrett Abram Alan Archibald Barnabas Bennett Brandon Branden Brenden Byron Byrne Thorburn Nathaniel

			Hugh Peter Ralph Robert Roger Simon Thomas Walter
			Alan Albert Alfred Andrew Anselm Arnold Baldrick David Edmund Edward Eric George Gerald Gilbert
			Gregory Humphrey Jordan Liel Mark Martin Matthew
			Randolph Reginald Stephen Teague Waleran

		}

		female_names = {
			#EU4
			Adra Alara Alina Amarien Aria Calassa Aucanna Castennia Castína Cela Celadora Clarimonde Corina Cora Coraline Eilís Eilísabet Erella Erela Galina 
			Galinda Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille 
			Kerstin Alisanne Isobel Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence Emilíe Gisele Athana

			#New
			Abigail Allison Annabel Belinda Bridget Deidre Heather Meade Melinda Rosamond Sybil 

			Agatha
			Margaret

			Beatrice Blanche Catherine Edith Ela Emma Isabel
			Isabella Juliana Margaret Mary Matilda Maud Sybilla

		}

		dynasty_of_location_prefix = "dynnp_sil"

		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 25
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 20
		mat_grm_name_chance = 40
		mother_name_chance = 5
		
		ethnicities = {
			20 = caucasian_blond
			30 = caucasian_brown_hair
			50 = caucasian_dark_hair
		}

		mercenary_names = {
			{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
			{ name = "mercenary_company_hawkwoods_band" }
		}
	}
	
	moorman = {
		graphical_cultures = {
			english_coa_gfx
		}
		
		color = { 71  84  102 }

		cadet_dynasty_names = {
			{ "dynnp_de" "dynn_Bohun" }
			{ "dynnp_de" "dynn_Vere" }
			{ "dynnp_de" "dynn_Gand" }
			"dynn_Myall"
			{ "dynnp_de" "dynn_Turberville" }
			{ "dynnp_de" "dynn_Holland" }
			"dynn_Butler"
			{ "dynnp_of" "dynn_Thornham" }
			{ "dynnp_de" "dynn_Camville" }
			"dynn_Breakspear"
			{ "dynnp_of" "dynn_Itchington" }
			{ "dynnp_of" "dynn_Northall" }
			{ "dynnp_de" "dynn_Montagu" }
			{ "dynnp_de" "dynn_Umfraville" }
			"dynn_FitzPeter"
			"dynn_Belles-mains"
			{ "dynnp_of" "dynn_Warwick" }
			"dynn_FitzAlan"
			{ "dynnp_de" "dynn_Beauchamp" }
			{ "dynnp_of" "dynn_Ilchester" }
			{ "dynnp_de" "dynn_Raleigh" }
			{ "dynnp_de" "dynn_Stratford" }
			"dynn_Becket"
			{ "dynnp_de" "dynn_la_Pole" }

		}

		dynasty_names = {
			{ "dynnp_de" "dynn_Bohun" }
			{ "dynnp_de" "dynn_Vere" }
			{ "dynnp_de" "dynn_Gand" }
			"dynn_Myall"
			{ "dynnp_de" "dynn_Turberville" }
			{ "dynnp_de" "dynn_Holland" }
			"dynn_Butler"
			{ "dynnp_of" "dynn_Thornham" }
			{ "dynnp_de" "dynn_Camville" }
			"dynn_Breakspear"
			{ "dynnp_of" "dynn_Itchington" }
			{ "dynnp_of" "dynn_Northall" }
			{ "dynnp_de" "dynn_Montagu" }
			{ "dynnp_de" "dynn_Umfraville" }
			"dynn_FitzPeter"
			"dynn_Belles-mains"
			{ "dynnp_of" "dynn_Warwick" }
			"dynn_FitzAlan"
			{ "dynnp_de" "dynn_Beauchamp" }
			{ "dynnp_of" "dynn_Ilchester" }
			{ "dynnp_de" "dynn_Raleigh" }
			{ "dynnp_de" "dynn_Stratford" }
			"dynn_Becket"
			{ "dynnp_de" "dynn_la_Pole" }
		}
		
		male_names = {
			#Vanilla - from Scottish
			Adam Alpin Alan Alastair Andrew
			Angus Archibald Arran Aulay Beathan Brian Brice Calum Carbrey Colban
			Colin Conall Conan David Dermid Donald Douglas Duff Dugald
			Duncan Edgar Edward Edwin Eric Evander Ewan Farquhar Fergus Fingal Findlay Frang
			Gavin Gilbert Gilbride Gillespie Gilpatrick Gilroy Giric Glenn Gregor
			Hamish Hector Hugh Iain Indulf James Kenneth Lachlan Laurence Lennon
			Lulach Malcolm Maldoven Maldred Malmure Marcas
			Matthew Michael Morgan Murdoch Murray Neil Ninian Oscar
			Patrick Radulf Ranald Richard Robert Roderick Ronald Rory Ross Roy
			Shaw Simon Somerled Stephan Sweeny Talore Thomas Torquil Walan
			Walter 


			#Vanilla - Added from others
			Jon Artur Nathaniel Byrne Byron
			
			#New
			Henric Heathcliff Hindley Hareton Linton Lockwood Bennett Campbell


		}
		female_names = {
			#Vanilla - from Scottish
			Ada Affraic Agnes Aileen Alice Anna Annabella Beitris Beathoc Bride
			Catriona Cecilia Christina Deirdre Derilla Donada Donella
			Edith Edna Effie Eilionoir Eimhir Eithne Ela Eleanor Elspeth Euna Eva Fenella Flora
			Forflissa Galiena Glenna Gormelia Helen Innes Iona Isabel Isla Isobel Julia
			Kenna Kirstin Lillias Lorna Malmure Maisie Malina Margaret Mariota Marjory Mary
			Martha Maud Maura Mirren Morag Morna Moyna Muriel
			Murron Nuala Rhonda Rodina Ronalda Ros Saundra Sheena Shona Slaine Sorcha Steacy Una

			#Vanilla - Added from others
			Catherine Isabella Ellen

			#New
			Cathy Frances
		}

		dynasty_of_location_prefix = "dynnp_of"
		bastard_dynasty_prefix = "dynnp_mur"
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 25
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 20
		mat_grm_name_chance = 40
		mother_name_chance = 5
		
		ethnicities = {
			20 = caucasian_blond
			10 = caucasian_brown_hair
			70 = caucasian_dark_hair
		}

		mercenary_names = {
			{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
			{ name = "mercenary_company_hawkwoods_band" }
		}
	}
	
	vertesker = {
		graphical_cultures = {
			english_coa_gfx
		}
		
		color = { 57  81  87 }

		cadet_dynasty_names = {
			{ "dynnp_de" "dynn_Bohun" }
			{ "dynnp_de" "dynn_Vere" }
			{ "dynnp_de" "dynn_Gand" }
			"dynn_Myall"
		}

		dynasty_names = {
			{ "dynnp_de" "dynn_Bohun" }
			{ "dynnp_de" "dynn_Vere" }
			{ "dynnp_de" "dynn_Gand" }
		}
		
		male_names = {

			#EU4
			Adelar Alain Alen Arnold Camor Carlan Clothar Colyn Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
			Frederic Godrac Godric Godryc Godwin Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Rycan Ricard Rogec 
			Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Vencan Walter Welyam Wystan
			Caylen Arman Adrien Adrian Canrec Camir Venac

			#New
			Alenn Alann Lennic Lennard Jon
			Garret Garrett Abram Alan Barnabas Bennett Brandon Branden Brenden Byron Byrne Thorburn Nathaniel

			Alan Albert Alfred Andrew Anselm Arnold Baldrick David Edmund Edward Eric Gilbert
			Gregory Humphrey Liel Mark Martin Matthew Michael Paul
			Randolph Reginald Teague Waleran

			#From Vanilla - this is from Castanorian btw
			Alaric Caradaig Caltram Aenbecan Domelch Drosten Garalt Maelchon Occo Folcbald Sibod Alarich Clotaire
			Bertold Bertram Boddic Dethard Eggerd Ekbert Ewald Hoger Hulderic Humfried Theoderic Theodoric
			Otto Kaspar Konrad Arnold Arnulf Rothard
			Avin Bernhard Meginulf Osnath Wambald
			Willem Steyn Alwin Barend Egmund Hugo Roland Leuderic
			Ceslin Chararic Charibert Childebert Childeric Chilperic Chlodomer Chlothar Chramnesind Clodio Clodion Clovis Creat

			Osric Morcar Sigeric Eadric
			
			Beorn Eadric Eardwulf Eastmund Guthmund Leofric Morcar Ordgar Osmund Osric
			Eadbald Eormenric

		}

		female_names = {
			#EU4
			Bella Clarya Constance Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
			Coraline Cora Corina Cecille Athana

			#New
			Abigail Allison Annabel Belinda Bridget Deidre Heather Meade Melinda Rosamond Sybil 

			#From Vanilla - this is from Castanorian btw
			Gisela Gisela Bertrada Bertilla Bertha Bertrada Gertrud Gisela Ida Sophia
			Helene Hemma Judith Ida Susanna
			Brethoc Claudia Candida Clementia Hildegarde Christine Ursula
			Alena Fenna Elke 
			Filippa Frida Gerda Godila Reynilde Maria Mathilde Oda Odelt Olinde Onna

			Agatha Balthild Judith Mildrith  
			Margaret

			Beatrice Blanche Catherine Edith Ela Emma Isabel
			Isabella Juliana Margaret Mary Matilda Maud Sybilla

		}

		dynasty_of_location_prefix = "dynnp_of"
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 25
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 20
		mat_grm_name_chance = 40
		mother_name_chance = 5
		
		ethnicities = {
			5 = caucasian_blond
			15 = caucasian_brown_hair
			80 = caucasian_dark_hair
		}

		mercenary_names = {
			{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
			{ name = "mercenary_company_hawkwoods_band" }
		}
	}
	
	wexonard = {
		graphical_cultures = {
			english_coa_gfx
		}
		
		color = { 97  0  137 }

		cadet_dynasty_names = {
			{ "dynnp_de" "dynn_Bohun" }
			{ "dynnp_de" "dynn_Vere" }
			{ "dynnp_de" "dynn_Gand" }
		}

		dynasty_names = {
			{ "dynnp_de" "dynn_Bohun" }
			{ "dynnp_de" "dynn_Vere" }
			{ "dynnp_de" "dynn_Gand" }
		}
		
		#Wexonard names are very much more Old Alenic type
		male_names = {

			#EU4
			Acromar Adelar Alain Alen Arnold Carlan Celgal Ciramod Clarimond Clothar Colyn Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
			Frederic Godrac Godric Godryc Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Rycan Ricard Rogec 
			Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Ulric Venac Vencan Walter Welyam Wystan Bellac
			Caylen Arman Adrien Adrian

			#New
			Alenn Alann Lennic Lennard Lothane Lothar Artur Artus Oto Ottog Ottron Ottomac Ottolin

			#Vanilla - from Old Saxon
			Abo Adalgar Adalgod Aelle Alebrand
			Altfrid Ansgar Arnd Arnold Asig Beneke Benno
			Bernhard Bertold Bertram Boddic Brun Cobbo Cord
			Dethard Detmar Donar Eggerd
			Erik Esiko Everd Ewald Freawine Gero Gerold Gevert
			Ghert Giselher Giselmar Goswin Hartwig
			Hengest Henneke Herberd Hinrik Hoger Hulderic Humfried Immed Jacob Jaspar Jochim
			Lembert Liemar Liudger
			Luder Ludwig Marbold Marquard Mathias Merten Michael
			Norbert Odo Ordulf Radke Reginbern Reinbern Reineke Reinmar Rudigar
			Rudolf Steffen Thankmar
			Theodwin Thimo Tobe Tymmeke Unwan Viric Volkwin Volrad Walbert Waldemar Waldered Waltard
			Warin Wenzel Werneke Wicho Wigebert Wilbrand
			Wulff

			Beorn Eadric Eardwulf Eastmund Guthmund Leofric Morcar Ordgar Osmund Osric Wulf Wulfgar Wulfgeat Wulfhelm Wulfhere Cenwulf Ceolwulf Beornwulf
			Werestan Eadbald Eormenric
		}

		female_names = {
			#EU4
			Clarimonde Clarya Clothilde Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Magda Matilda Adela Edith Elenor Emma
			Cora Corina Cecille Valence Athana

			#old saxon
			Addila Adelheid Alof Agnes Anna Athela Beatrix Bertha Bertrada Bia Bisina Bithild
			Brigida Christina Diedke Eilika Elisabeth Enda Frederuna Geilana Gerberga Gertrud Gisela
			Glismod Hadwig Hasala Heilwig Helene Hildegard Ida Imma Irmgard Irminburg
			Jutta Katharina Kunigunde Luitgard Margarete Mathilde Mechthild

			#For Gawedi
			Agatha Balthild Ecgfrida Judith Mildrith  
			Wulfhild Margaret Wulfwynn 	 

		}

		dynasty_of_location_prefix = "dynnp_of"
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 25
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 20
		mat_grm_name_chance = 40
		mother_name_chance = 5
		
		ethnicities = {
			50 = caucasian_blond
			25 = caucasian_brown_hair
			25 = caucasian_dark_hair
		}

		mercenary_names = {
			{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
			{ name = "mercenary_company_hawkwoods_band" }
		}
	}

	#Elvenized Wex - NON CANON!
	thilosian = {
		graphical_cultures = {
			english_coa_gfx
		}
		
		color = { 97  0  137 }

		cadet_dynasty_names = {
			{ "dynnp_de" "dynn_Bohun" }
			{ "dynnp_de" "dynn_Vere" }
			{ "dynnp_de" "dynn_Gand" }
			"dynn_Myall"
		}

		dynasty_names = {
			{ "dynnp_de" "dynn_Bohun" }
			{ "dynnp_de" "dynn_Vere" }
			{ "dynnp_de" "dynn_Gand" }
			"dynn_Myall"
			{ "dynnp_de" "dynn_Turberville" }
			{ "dynnp_de" "dynn_Holland" }
		}
		
		#Wexonard names are very much more Old Alenic type
		male_names = {

			#EU4
			Adelar Alain Alen Arnold Camor Carlan Colyn Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
			Godric Henric Humban Humbar Humbert Lain Lan Madalac Marcan Petran 
			Stovan Teagan Toman Tomar Venac Vencan Walter Welyam

			Arman Adrian Ciramod Clothar Ricard Ottrac Ottran Lain Lan Godric

			#New
			Alenn Alann Lennic Lennard Lothane Lothar Artur Artus Oto

			#Vanilla - from Old Saxon
			Abo Adalgar Adalgod Aelle Alebrand
			Altfrid Ansgar Arnd Arnold Asig Beneke Benno
			Bernhard Bertold Bertram Boddic Brun Cobbo Cord
			Dethard Detmar Donar Eggerd
			Erik Esiko Everd Ewald Freawine Gero Gerold Gevert
			Ghert Giselher Giselmar Goswin Hartwig
			Hengest Henneke Herberd Hinrik Hoger Hulderic Humfried Immed Jacob Jaspar Jochim
			Lembert Liemar Liudger
			Luder Ludwig Marbold Marquard Mathias Merten Michael
			Norbert Odo Ordulf Radke Reginbern Reinbern Reineke Reinmar Rudigar
			Rudolf Steffen Thankmar
			Theodwin Thimo Tobe Tymmeke Udo Unwan Viric Volkwin Volrad Walbert Waldemar Waldered Waltard
			Warin Wenzel Werneke Wicho Wigebert Wilbrand
			Wulff

			Beorn Eadric Eardwulf Eastmund Guthmund Leofric Morcar Ordgar Osmund Osric Wulf Wulfgar Wulfgeat Wulfhelm Wulfhere Cenwulf Ceolwulf Beornwulf
			Werestan Eadbald Eormenric
		}

		female_names = {
			#EU4
			Adra Alara Alina Amarien Aria Calassa Aucanna Castennia Castína Cela Celadora Clarimonde Corina Cora Coraline Eilís Eilísabet Erella Erela Galina 
			Galinda Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille 
			Kerstin Alisanne Isobel Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence Emilíe Gisele Athana

			#old saxon
			Addila Adelheid Alof Agnes Anna Athela Bertha Bertrada Bia Bisina Bithild
			Brigida Diedke Eilika Elisabeth Enda Frederuna Geilana Gerberga Gertrud Gisela
			Glismod Hadwig Hasala Heilwig Helene Hildegard Ida Imma Irmgard Irminburg
			Jutta Katharina Kunigunde Luitgard Margarete Mathilde Mechthild

			#For Gawedi
			Agatha Balthild Ecgfrida Judith Mildrith  
			Wulfhild Margaret Wulfwynn 	 

		}
		dynasty_of_location_prefix = "dynnp_sil"
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 25
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 20
		mat_grm_name_chance = 40
		mother_name_chance = 5
		
		ethnicities = {
			50 = caucasian_blond
			25 = caucasian_brown_hair
			25 = caucasian_dark_hair
		}

		mercenary_names = {
			{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
			{ name = "mercenary_company_hawkwoods_band" }
		}
	}
}



