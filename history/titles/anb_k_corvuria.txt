k_corvuria = {
	1020.1.1 = {
		liege = "e_bulwar"
		holder = 113 #Denarion Denarzuir
	}
}

d_bal_dostan = {
	1020.1.1 = {
		holder = 113 #Denarion Denarzuir
		liege = "k_corvuria"
	}
}

c_arca_corvur = {
	1020.1.1 = {
		holder = 113 #Denarion Denarzuir
		liege = "d_bal_dostan"
	}
}

c_mihaelas_redoubt = {
	1020.1.1 = {
		holder = 113 #Denarion Denarzuir
		liege = "d_bal_dostan"
	}
}

c_elderwright = {
	1020.1.1 = {
		holder = 113 #Denarion Denarzuir
		liege = "d_bal_dostan"
	}
}

c_silcorvur = {
	1020.1.1 = {
		holder = 113 #Denarion Denarzuir
		liege = "d_bal_dostan"
	}
}

d_blackwoods = {
	990.1.7 = {
		holder = 106 #Kolvan Karnid
	}
	1020.1.1 = {
		liege = "k_corvuria"
	}
}

c_blackwoods = {
	990.1.7 = {
		holder = 106 #Kolvan Karnid
	}
	1020.1.1 = {
		liege = "d_blackwoods"
	}
}

c_kortir = {
	990.1.7 = {
		holder = 106 #Kolvan Karnid
	}
	1020.1.1 = {
		liege = "d_blackwoods"
	}
}

c_karns_hold = {
	990.1.7 = {
		holder = 106 #Kolvan Karnid
	}
	1020.1.1 = {
		liege = "d_blackwoods"
	}
}

c_holstead = {
	990.1.7 = {
		holder = 106 #Kolvan Karnid
	}
	1020.1.1 = {
		liege = "d_blackwoods"
	}
}

d_tiferben = {
	997.8.14 = {
		holder = 114 #Petre Tiferben
	}
	1020.1.1 = {
		liege = "k_corvuria"
	}
}

c_shroudfort = {
	997.8.14 = {
		holder = 114 #Petre Tiferben
	}
	1020.1.1 = {
		liege = "d_tiferben"
	}
}

c_marchfield = {
	997.8.14 = {
		holder = 114 #Petre Tiferben
	}
	1020.1.1 = {
		liege = "d_tiferben"
	}
}

c_ioans_ford = {
	997.8.14 = {
		holder = 114 #Petre Tiferben
	}
	1020.1.1 = {
		liege = "d_tiferben"
	}
}

c_aelaintaire = {
	997.8.14 = {
		holder = 114 #Petre Tiferben
	}
	1020.1.1 = {
		liege = "d_tiferben"
	}
}

c_cannvalley = {
	997.8.14 = {
		holder = 114 #Petre Tiferben
	}
	1020.1.1 = {
		liege = "d_tiferben"
	}
}

c_rackmans_court = {
	997.8.14 = {
		holder = 114 #Petre Tiferben
	}
	1020.1.1 = {
		liege = "d_tiferben"
	}
}

d_ravenhill = {
	1021.1.1 = {
		holder = 120 #Vasile sil Fiachlar
		liege = "k_corvuria"
	}
}

c_ravenhill = {
	1021.1.1 = {
		holder = 120 #Vasile sil Fiachlar
		liege = "d_ravenhill"
	}
}

c_cannmarionn = {
	1021.1.1 = {
		holder = 120 #Vasile sil Fiachlar
		liege = "d_ravenhill"
	}
}

c_gablaine = {
	1021.1.1 = {
		holder = 120 #Vasile sil Fiachlar
		liege = "d_ravenhill"
	}
}

c_arca_kaldere = {
	1021.1.1 = {
		holder = 120 #Vasile sil Fiachlar
		liege = "d_ravenhill"
	}
}