﻿object={
	name="waterfall"
	clamp_to_water_level=yes
	render_under_water=no
	generated_content=no
	layer="env_effect_layer"
	entity="waterfall"
	count=2
	transform="1585.350342 2.947510 2541.591797 0.000000 0.575566 0.000000 0.817755 1.000000 1.000000 1.000000
1638.546753 3.583740 3754.739990 0.000000 0.215535 0.000000 0.976496 1.000000 1.000000 1.000000
"}
object={
	name="waterfall_bottom"
	clamp_to_water_level=yes
	render_under_water=no
	generated_content=no
	layer="env_effect_layer"
	entity="waterfall_bottom"
	count=2
	transform="1583.304688 0.000000 2539.592285 0.000000 0.000000 0.000000 1.000000 1.000000 1.000000 1.000000
1638.324097 0.206543 3752.261475 0.000000 0.000000 0.000000 1.000000 1.000000 1.000000 1.000000
"}
