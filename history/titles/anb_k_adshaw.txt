k_adshaw = {
	995.3.4 = {
		holder = 62 #Rylen Adshaw
	}
}

d_celmaldor = {
	1014.3.2 = {
		holder = 61 #Maldorian the Navigator
		government = republic_government
	}
}

d_serpentgard = {
	990.2.9 = {
		holder = 63 #Rikkard Serpentsgard
	}
}

d_baywic = {
	1017.5.4 = {
		holder = 64 #Mason Cobbler
		government = republic_government
	}
}